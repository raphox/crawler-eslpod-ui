(function () {
  'use strict';

  angular
    .module('app.podcast')
    .component('podcastList', {
      require: {
        app: '^app'
      },
      templateUrl: 'app/podcast/podcast-list.component.html',
      controller: PodcastListController,
      transclude: true
    });

  /** @ngInject */
  function PodcastListController(podcastFactory, $window) {
    var moment = $window.moment;
    var vm = this;

    vm.podcasts = [];
    vm.getSong = getSong;

    // Available methods

    init();

    /**
    * Initialize
    */
    function init() {
      return podcastFactory
        .getList({include: 'tags'})
        .then(function (data) {
          angular.forEach(data, function (item) {
            vm.podcasts.push(vm.getSong(item));
          });

          return vm.podcasts;
        });
    }

    function getSong(podcast) {
      var attributes = podcast.attributes;
      var createdAt = moment.utc(attributes['created-at']);

      return {
        id: podcast.id,
        title: attributes.title,
        category: attributes.category,
        number: attributes.number,
        description: attributes.description,
        url: attributes.path,
        createdAt: createdAt,
        createdAtMinutes: moment().diff(createdAt, "minutes")
      };
    }
  }
})();

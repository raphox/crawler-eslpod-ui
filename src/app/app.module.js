(function () {
  'use strict';

  angular
    .module('app', [
      'app.core',
      'app.podcast'
    ]);
})();

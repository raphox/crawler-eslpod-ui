(function () {
  'use strict';

  angular
    .module('app.core')
    .config(config);

  /** @ngInject */
  function config(RestangularProvider, cfpLoadingBarProvider, $mdThemingProvider, $windowProvider) {
    var $window = $windowProvider.$get();
    var _ = $window._;

    // TODO: Move to a config file
    RestangularProvider.setBaseUrl('https://crawler-eslpod-raphox.c9users.io:8080');
    RestangularProvider.setRequestSuffix('.json');

    RestangularProvider.addResponseInterceptor(function (data) {
      var extractedData = data.data;

      function _apply(item) {
        return _.extend(item, {
          getIncluded: function () {
            return _.find(data.included, {type: item.type, id: item.id});
          }
        });
      }

      _.each(data.data, function (elem) {
        _.each(elem.relationships, function (rel) {
          _.each(rel.data, _apply);
        });
      });

      return extractedData;
    });

    cfpLoadingBarProvider.includeSpinner = false;

    $mdThemingProvider.theme('success');
    $mdThemingProvider.theme('danger');
  }
})();

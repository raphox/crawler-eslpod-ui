(function () {
  'use strict';

  angular
    .module('app')
    .component('toolbar', {
      templateUrl: 'app/toolbar.component.html',
      controller: ToolbarController
    });

  /** @ngInject */
  function ToolbarController() {
  }
})();

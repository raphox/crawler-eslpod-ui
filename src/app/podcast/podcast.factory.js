(function () {
  'use strict';

  angular
    .module('app.podcast')
    .factory('podcastFactory', podcastFactory);

  /** @ngInject */
  function podcastFactory(Restangular) {
    return Restangular.service('podcasts');
  }
})();

(function () {
  'use strict';

  angular
    .module('app.podcast')
    .component('podcastPlayer', {
      require: {
        app: '^app'
      },
      templateUrl: 'app/podcast/podcast-player.component.html',
      controller: PodcastPlayerController,
      transclude: true
    });

  /** @ngInject */
  function PodcastPlayerController() {
    // var vm = this;

    // Available methods

    init();

    /**
    * Initialize
    */
    function init() {
    }
  }
})();

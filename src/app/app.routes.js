(function () {
  'use strict';

  angular
    .module('app')
    .config(config);

  /** @ngInject */
  function config($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('app', {
        url: '/',
        views: {
          'main@': {
            component: 'app'
          },
          'toolbar@app': {
            component: 'toolbar'
          },
          'content@app': {
            component: 'podcastList'
          }
        }

      });
  }
})();

(function () {
  'use strict';

  angular
    .module('app')
    .component('app', {
      templateUrl: 'app/app.component.html',
      controller: AppController
    });

  /** @ngInject */
  function AppController() {
    var vm = this;

    vm.getTag = getTag;

    function getTag(podcast) {
      return podcast && podcast.category === 'English Café' ? 'cafe' : 'pod';
    }
  }
})();
